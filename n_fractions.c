//WAP to find the sum of n fractions.

#include<stdio.h>
typedef struct fraction
{
  int num;
  int deno;
} Fraction;
int
no_of_frac ()
{
  int n;
  printf ("Enter the number of fractions to be added:");
  scanf ("%d", &n);
  return n;
}

Fraction
input_one_fraction (int n, Fraction arr[n])
{
  for (int j = 0; j < 1; ++j)
    {
      printf ("Enter the fraction 1 numerator and denominator):");
      scanf ("%d%d", &arr[j].num, &arr[j].deno);
    }
}

Fraction
input_fraction (int n, Fraction arr[n])
{
  for (int i = 1; i < n; ++i)
    {
      printf ("Enter the fraction %d 's numerator and denominator):", i + 1);
      scanf ("%d%d", &arr[i].num, &arr[i].deno);
    }
}

Fraction
sum (int n, Fraction arr[n])
{
  Fraction r;
  Fraction temp;
  int i;
  int numerator = 0, denominator = 1;
  for (i = 0; i < n; i++)
    {
      denominator *= arr[i].deno;
    }
  for (i = 0; i < n; i++)
    {
      numerator += (arr[i].num) * (denominator / arr[i].deno);
    }
  temp.num = numerator;
  temp.deno = denominator;
  return temp;
}

Fraction
gcd (int n, Fraction arr[n])
{
  Fraction r;
  Fraction temp;
  int i;
  int gcd;
  for (i = 1; i <= temp.num && i <= temp.deno; i++)
    {
      if (temp.num % i == 0 && temp.deno % i == 0)
	{
	  gcd = i;
	}
    }
  r.num = temp.num / gcd;
  r.deno = temp.deno / gcd;
  return r;
}

void
result (int n, Fraction arr[n], Fraction r)
{
  int i;
  printf ("The sum of ");
  for (i = 0; i < n - 1; ++i)
    {
      printf ("%d/%d+", arr[i].num, arr[i].deno);
    }
  printf ("%d/%d=%d/%d", arr[i].num, arr[i].deno, r.num, r.deno);
}

int
main ()
{
  int n;
  Fraction fraction_result;
  Fraction arr[n];
  n = no_of_frac ();
  input_one_fraction (n, arr);
  input_fraction (n, arr);
  sum (n, arr);
  fraction_result = gcd (n, arr);
  result (n, arr, fraction_result);
  return 0;
}