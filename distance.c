//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
float coordinate()
{
	float x;
	printf("Enter the coordinate: ");
	scanf("%f", &x);
	
	return x;
}

float find_distance(float x1, float y1, float x2, float y2)
{
	float dist;
	dist = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));

	return dist;
}

float output(float dist)
{

	printf("The distance is %f", dist);
}



int main()
{
	float x1,y1,x2,y2,distance;
	x1= coordinate();
    y1= coordinate();
    x2= coordinate();
    y2= coordinate();
    distance = find_distance(x1,y1,x2,y2);
    output(distance);
return 0;
}
